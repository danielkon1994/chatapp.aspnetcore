using System.Collections.Generic;
using System.Threading.Tasks;
using ChatApp.AspNetCore.Model;
using ChatApp.AspNetCore.Services;
using Microsoft.AspNetCore.SignalR;

namespace ChatApp.AspNetCore.Hubs
{
    public class ChatHub : Hub
    {
        private IUserInMemoryService _userInMemoryService;

        public ChatHub(IUserInMemoryService userInMemoryService)
        {
            _userInMemoryService = userInMemoryService;
        }

        public async Task Join(string userName) {
            string connectionId = Context.ConnectionId;
            if(!_userInMemoryService.AddUpdate(connectionId, userName))
                await Clients.AllExcept(new List<string>{connectionId}).SendAsync("NewUserJoined", _userInMemoryService.GetUserInfoByName(userName));
            
            await Clients.Client(connectionId).SendAsync("Joined", _userInMemoryService.GetUserInfoByName(userName));

            await Clients.Client(connectionId).SendAsync("OnlineUser", _userInMemoryService.GetAllUsers(userName));
        }

        public async Task Leave(string userName) {
            string connectionId = Context.ConnectionId;
            
            _userInMemoryService.Remove(userName);
            
            await Clients.AllExcept(connectionId).SendAsync("Departed", userName);
        }

        public async Task SendMessage(string userName, ChatMessage message)
        {
            var fromUser = _userInMemoryService.GetUserInfoByConnectionId(Context.ConnectionId);
            var toUser = _userInMemoryService.GetUserInfoByName(userName);
            
            await Clients.Client(toUser.ConnectionId).SendAsync("SendMessageFrom", message, fromUser);
            await Clients.Client(fromUser.ConnectionId).SendAsync("SendMessageTo", message, toUser);
        }
    }
}