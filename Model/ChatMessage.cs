using System;

namespace ChatApp.AspNetCore.Model
{
    public class ChatMessage
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}