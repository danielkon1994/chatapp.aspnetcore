namespace ChatApp.AspNetCore.Model
{
    public class UserDetail
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}