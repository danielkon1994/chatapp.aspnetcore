using System.Collections.Generic;
using ChatApp.AspNetCore.Model;

namespace ChatApp.AspNetCore.Services
{
    public interface IUserInMemoryService
    {
         bool AddUpdate(string connectionId, string name);
         void Remove(string name);
         IEnumerable<UserDetail> GetAllUsers(string exceptName);
         UserDetail GetUserInfoByConnectionId(string connectionId);
         UserDetail GetUserInfoByName(string name);
    }
}