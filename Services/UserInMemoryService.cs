using System.Collections.Generic;
using ChatApp.AspNetCore.Model;
using System.Collections.Concurrent;
using System.Linq;

namespace ChatApp.AspNetCore.Services
{
    public class UserInMemoryService : IUserInMemoryService
    {
        private ConcurrentDictionary<string, UserDetail> _onlineUsers = new ConcurrentDictionary<string, UserDetail>();

        public bool AddUpdate(string connectionId, string name)
        {
            var userAlreadyExists = _onlineUsers.ContainsKey(name);

            var userDetail = new UserDetail {
                ConnectionId = connectionId,
                UserName = name
            };

            _onlineUsers.AddOrUpdate(name, userDetail, (key, value) => userDetail);

            return userAlreadyExists;
        }

        public IEnumerable<UserDetail> GetAllUsers(string exceptName)
        {
            return _onlineUsers.Values.Where(i => i.UserName != exceptName);
        }

        public UserDetail GetUserInfoByConnectionId(string connectionId)
        {
            return _onlineUsers.Values.FirstOrDefault(i => i.ConnectionId == connectionId);
        }

        public UserDetail GetUserInfoByName(string name)
        {
            return _onlineUsers.Values.FirstOrDefault(i => i.UserName == name);
        }

        public void Remove(string name)
        {
            UserDetail userDetail;
            _onlineUsers.TryRemove(name, out userDetail);
        }
    }
}